package com.ms.service;

import com.ms.model.Category;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CategoryService {
    private final RestTemplate restTemplate;
    private final String apiUrl="https://api.escuelajs.co/api/v1/categories";

    public CategoryService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
      public Category[] getAllCategory() {
        return restTemplate.getForObject(apiUrl, Category[].class);
    }
    public Category getCategoryById(String id) {
        return restTemplate.getForObject(apiUrl+id, Category.class);
    }
    
}
