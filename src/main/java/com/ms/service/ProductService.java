package com.ms.service;

import com.ms.dto.ProductDto;
import com.ms.model.Product;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductService {
    
    private final String apiUrl ="https://api.escuelajs.co/api/v1/products";
    
    private final RestTemplate restTemplate;

    public ProductService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ProductDto[] getAllProducts() {
        return restTemplate.getForObject(apiUrl, ProductDto[].class);
    }

    public Product createProduct(Product product) {
        return restTemplate.postForObject(apiUrl, product, Product.class);
    }

}
