# Prueba Nexsys

Este proyecto es una aplicación web desarrollada en Java que ofrece una serie de servicios RESTful. Aquí se detallan los pasos necesarios para ejecutar y probar la aplicación.

## Requisitos previos

Antes de ejecutar la aplicación, asegúrate de tener instalado Java 17 en tu sistema.

## Clonar el repositorio

Puedes clonar el repositorio del proyecto ejecutando el siguiente comando en tu terminal:


git clone https://gitlab.com/-/ide/project/chalamaicol9/pruebanexsys

## Configurar el proyecto
Una vez clonado el repositorio, abre el proyecto con tu editor de preferencia. Asegúrate de tener configurado el entorno de desarrollo correctamente.

## Ejecutar el proyecto
Para arrancar el proyecto, sigue estos pasos:

- Abre una terminal.
- Navega hasta el directorio del proyecto clonado.
- Ejecuta el siguiente comando: ./mvnw spring-boot:run


Esto iniciará la aplicación y estará disponible en la dirección http://localhost:8080.


## Servicios Disponibles
GET-POST para productos: http://localhost:8080/nexsys/v1/products/
GET para categorías: http://localhost:8080/nexsys/v1/categories/

Mas informacion en  la documentación de https://fakeapi.platzi.com/en/about/introduction/